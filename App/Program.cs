﻿using System;
//compressing: GZipTest.exe compress [original file name] [archive file name]
//decompressing: GZipTest.exe decompress [archive file name] [decompressing file name]
class Program {
	private static readonly string helpText = "Possible inputs:\n" +
		"compressing: GZipTest.exe compress [original file name] [archive file name]\n" +
		"decompressing: GZipTest.exe decompress [archive file name] [decompressing file name]\n";
	static int Main(string[] args) {
		Console.WriteLine("Veeam Technical Test");

		Settings? settings = ParseArgs(args);
		if (settings == null) {
			Console.WriteLine("Error: Input parameters are not corrected");
			Console.WriteLine(helpText);
			Console.ReadKey();
			return 1;
		} else {
			return RunApplication((Settings)settings);
		}
	}
	private static int RunApplication(Settings settings) {
		var watch = System.Diagnostics.Stopwatch.StartNew();

		bool result = false;
		switch (settings.executionType) {
			case Settings.ExecutionType.compress:
				result = Compressor.SimpleCompress(settings.firstFileName, settings.secondFileName);
				break;
			case Settings.ExecutionType.decompress:
				result = Compressor.SimpleDecompress(settings.firstFileName, settings.secondFileName);
				break;
		}

		watch.Stop();
		var elapsedMs = watch.ElapsedMilliseconds;
		Console.WriteLine($"Execution time is {elapsedMs} Ms");
		Console.ReadKey();
		return (result == true) ? 0 : 1;

	}
	private struct Settings {
		public enum ExecutionType { compress, decompress };
		public string firstFileName, secondFileName;
		public ExecutionType executionType;
	}
	private static Settings? ParseArgs(string[] args) {
		if (args.Length != 3) return null;
		Settings settings;
		if (args[0].ToLower() == "compress")
			settings.executionType = Settings.ExecutionType.compress;
		else if (args[0].ToLower() == "decompress")
			settings.executionType = Settings.ExecutionType.decompress;
		else
			return null;
		settings.firstFileName = args[1];
		settings.secondFileName = args[2];
		return settings;
	}
}