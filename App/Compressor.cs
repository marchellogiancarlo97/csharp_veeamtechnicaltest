using System;
using System.IO;
using System.IO.Compression;

public class Compressor {
	public static bool SimpleCompress(string inputFile, string outputFile) {
		if (!EvaluateParameters("SimpleCompress", inputFile, outputFile))
			return false;

		FileInfo fileToCompress = new FileInfo(inputFile);
		FileStream originalFileStream = fileToCompress.OpenRead();
		FileStream compressedFileStream = File.Create(outputFile);
		using (GZipStream compressionStream = new GZipStream(compressedFileStream,
						   CompressionMode.Compress)) {
			originalFileStream.CopyTo(compressionStream);
		}
		originalFileStream.Close();
		compressedFileStream.Close();
		Console.WriteLine($"Compressed file {fileToCompress} to {outputFile}");
		return true;
	}
	public static bool SimpleDecompress(string inputFile, string outputFile) {
		if (!EvaluateParameters("SimpleDecompress", inputFile, outputFile))
			return false;

		FileInfo fileToDecompress = new FileInfo(inputFile);
		FileStream originalFileStream = fileToDecompress.OpenRead();
		using (FileStream decompressedFileStream = File.Create(outputFile)) {
			using (GZipStream decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress)) {
				decompressionStream.CopyTo(decompressedFileStream);
				// Console.WriteLine($"Decompressed: {fileToDecompress.Name}");
			}
		}
		Console.WriteLine($"Decompressed file {inputFile} to {outputFile}");
		return true;
	}
	private static bool EvaluateParameters(string methodName, string inputFile, string outputFile) {
		if (!File.Exists(inputFile)) {
			Console.WriteLine($"[{methodName}]Error: input file not available");
			return false;
		}
		if (File.Exists(outputFile)) {
			Console.WriteLine($"[{methodName}]Warning: outputFile file already present");
			File.Delete(outputFile);
			return true;
		}
		return true;
	}
}